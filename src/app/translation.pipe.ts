import {Inject, Pipe, PipeTransform} from '@angular/core';
import {BS_TRANSLATION} from "./bs-translation.service";

@Pipe({
  name: 'translation'
})
export class TranslationPipe implements PipeTransform {

  constructor(@Inject(BS_TRANSLATION) private map: any){
  }

  transform(key: string): string {
    return this.map[key];
  }

}
