import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule} from "@angular/common/http";
import {BS_TRANSLATION, BsTranslationService} from "./bs-translation.service";
import { TranslationPipe } from './translation.pipe';


export function appLoadFactory(securityService: BsTranslationService) {
  return () => {
    return securityService.load();
  };
}

@NgModule({
  declarations: [
    AppComponent,
    TranslationPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],

  // https://medium.com/better-programming/how-to-handle-async-providers-in-angular-51884647366
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: appLoadFactory,
      deps: [BsTranslationService],
      multi: true,
    },{
    provide: BS_TRANSLATION,
    useFactory: (bsTranslationService: BsTranslationService):Map<string, string> => bsTranslationService.get(),
    deps: [BsTranslationService]
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
