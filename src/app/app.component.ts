import {Component, Inject} from '@angular/core';
import {BS_TRANSLATION} from "./bs-translation.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-initializer';

  testValue: string = 'bs_constant_capital_trad';

  constructor(){
  }
}
