import {Injectable, InjectionToken} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {tap} from "rxjs/operators";

export const BS_TRANSLATION: InjectionToken<Map<string, string>> = new InjectionToken<Map<string, string>>('BsTranslation');

@Injectable({
  providedIn: 'root'
})
export class BsTranslationService {

  private bstranslation: Map<string, string>;

  constructor(private  http: HttpClient) {
  }

  public load(): Promise<any>{
    return this.http.get<Map<string,string>>('assets/translations/bstranslation.json')
      .pipe(tap((data: Map<string,string>) => this.bstranslation = data)).toPromise();
  }

  public get(): Map<string, string> {
    return this.bstranslation;
  }
}
